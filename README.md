## Magento 2.3.2 Fix Error Admin Panel Blank Screen - Icon, JS, CSS not fully loaded on Windows
---
### Step by step
###### 1. Update `Validator.php`
- Buka folder `vendor/magento/framework/View/Element/Template/File/`
- Cari file `Validator.php`
- Cari function `isPathInDirectories`
- Ubah kode dari
``` php
        if (!is_array($directories)) {
            $directories = (array)$directories;
        }
        foreach ($directories as $directory) {
            if (0 === strpos($path, $directory)) {
                return true;
            }
        }
```
- menjadi seperti dibawah ini
```php
        if (!is_array($directories)) {
            $directories = (array)$directories;
        }
        $realPath = $this->fileDriver->getRealPath($path);
        $realPath = str_replace('\\', '/', $realPath); // extra code added
        foreach ($directories as $directory) {
            if (0 === strpos($realPath, $directory)) {
                return true;
            }
        }
        return false;
```
- **Step pertama selesai**

###### 2. Update `di.xml`
- Buka folder `app/etc/`
- Cari file `di.xml`
- Cari kode
```php
<item name="view_preprocessed" xsi:type="object">Magento\Framework\App\View\Asset\MaterializationStrategy\Symlink</item> 
```
- Ubah menjadi
```php
<item name="view_preprocessed" xsi:type="object">Magento\Framework\App\View\Asset\MaterializationStrategy\Copy</item> 
```
- **Selesai**
